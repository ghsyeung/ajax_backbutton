require 'rubygems'
require 'rack'

class Hello 
  REQ = "REQUEST_URI"
  def call(env)
    p env
    if env.has_key? Hello::REQ 
      if env[Hello::REQ] == "/s"
        body = File.open("s.html", "r+").read
      elsif env[Hello::REQ] == "/s.js"
        body = File.open("s.js", "r+").read
      elsif env[Hello::REQ] == "/400"
        return [404, {}, nil]
      else
        sleep(10)
        body = "Hello!"
      end
      [200, { "Content-Type" => "text/html" }, body]
    end
  end
end


Rack::Handler::Mongrel.run Hello.new, :Port => 9292
